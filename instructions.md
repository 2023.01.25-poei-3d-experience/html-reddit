# Reddit

## Menu

1. Ecrire en html dans `<nav>`{.html} :
    - un titre "Feeds" (`<h3>`{.html} ?)
    - un lien contenant :
        -  icon
        - "Popular"
    - un titre "Topics" (`<h3>`{.html} ?)
    - un lien contenant :
        -  icon
        - "Gaming"
        - icon
    - un lien contenant :
        -  icon
        - "Sports"
        - icon
    - un lien contenant :
        -  icon
        - "Business ..."
        - icon
    - ...
2. Faire du css :
    1. définir la police de manière globale
        - selecteur `*`
        - changer `font-family`
    2. styler les titres 
        - quel selecteur ? classe ? `nav > h3`{.css} ? 
        - changer la couleur (`color`), la police (`font-family`, `font-size`, `font-weight`, ...), marge ou padding (`margin`, `padding`), ...
    3. styler les liens
        - virer les styles par défaut 
            - souligné : `text-decoration`
            - couleur : `color`
            - couleur visité (selecteur `:visited`)
        - configurer la disposition des elements :
            - le `<a>` est une flexbox (`display: flex;`{.css})
            - regrouper (dans le html) le premier icon et le label dans une `<div>`{.html}
            - ajouter un `justify-content: space-between;`{.css}
        - ajouter nos propres styles :
            - un padding
            - changer la couleur de fond lors du survol (`background-color`)

## Posts

1. Ecrire en html dans `<div id="main">`{.html}, pour chaque post ajouter une `<article>`{.html} avec à l'interieur :
    - une `<div>` pour les votes :
        - un bouton (icon upvote)
        - le nombre de votes
        - un autre bouton (icon downvote)
    - une `<div>` avev le header contenant :
        - l'image du "channel" reddit
        - le lien du "channel"
        - un texte "Posted by `<user>` `<X>` time ago" où `<user>` est un lien avec le nom de l'utilisateur.
        - un bouton "Join"
    - une `<div>`{.html} avec le contenu :
        - un titre, suivi par des tags (qui sont des liens)
        - du contenu (image ? texte ?)
    - une `<div>`{.html} avec les actions :
        - un bouton comments (icon + nombre de commentaires)
        - un bouton "Share"
        - un bouton "Save"
        - un bouton ...
2. Faire du css :
    - organisation globale de l'article
        - l'article devient une grid (`display: grid` et définir les colonnes et les lignes puis `grid-template-...`)
        - placer les `div` filles de larticle dans les bonnes cases de la grid (`grid-area` ou `grid-column` et `grid-row`)
    - pour les votes :
        - une couleur de fond (`background-color`)
        - organiser le contenu en colonne, en haut (flex box !)
        - virer le style original des boutons :
            - pas de bordure (`border: none`)
            - bas de couleur de fond (`background-color: none`)
        - ajouter notre propre style :
            - boutons au survol : couleur de fond + couleur rouge ou bleu
            - nombre vote : en gras (`font-weight`)
    - pour le header :
        - organiser le contenu en ligne (flex box !)
        - regrouper toute la partie gauche dans une div ou un span
        - virer le style par defaut :
            - des liens (voir header)
            - du bouton
        - ajouter notre propre style :
            - le lien vers le channel en gras
            - en gris le reste de la phrase
            - souligner les liens lors du survol
            - bouton avec un fond bleu, les coins arrondis et changement de couleur lors du survol

## Menu avec dropdown

1. Ajouter des sous-menus en html et les styler correctement
    - ajouter un contaeneur (une div) autour des items du menu
    - mettre à jour le CSS pour que ce soit correct
    - dans ce conteneur, ajouter les sous menus (une `<div>` contenant des `<a>`)
2. Afficher ou cacher les sous-menu
    - en CSS que les sous-menus soit cachés (`height` à 0)
    - afficher les sous-menus si le conteneur à la class `expanded`
3. En js
    - ajouter des events listener sur le click sur les entrées du menu
    - lors du click : ajoute ou enleve (toggle) la classe `expanded` sur le parent de l'entrée du menu