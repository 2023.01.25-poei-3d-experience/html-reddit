

let loginButton = document.querySelector("#login-button");


loginButton.addEventListener("click", (evt) => {
    let icon = document.querySelector(".header-home-icon");
    icon.style.backgroundColor = "blue";
})


let upVoteButtons = document.querySelectorAll(".article-votes > button:first-child");
let downVoteButtons = document.querySelectorAll(".article-votes > button:last-child");
let voteSpans = document.querySelectorAll(".article-votes > span");

for (let button of upVoteButtons) {
    button.addEventListener("click", evt => {
        let span = button.nextElementSibling;
        span.textContent = parseInt(span.textContent) + 1;
    });
}

for (let button of downVoteButtons) {
    button.addEventListener("click", evt => {
        let span = button.previousElementSibling;
        span.textContent = parseInt(span.textContent) - 1;
    });
}


let deleteButtons = document.querySelectorAll(".delete-article");
for (let button of deleteButtons) {
    button.addEventListener("click", evt => {
        button.parentElement.parentElement.remove();
    });
    button.addEventListener("mouseenter", evt => {
        button.style.top = (Math.random()*200-100) + "px";
        button.style.right = (Math.random()*200-100) + "px";
    });
}


let items = document.querySelectorAll("#nav .item");
for (let item of items) {
    item.addEventListener("click", evt => {
        item.parentElement.classList.toggle("expanded");
    });
}